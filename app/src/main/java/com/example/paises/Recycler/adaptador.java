package com.example.paises.Recycler;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.paises.R;
import com.example.paises.detalle;
import com.example.paises.enetity.pais;
import com.squareup.picasso.Picasso;

import java.util.List;

public class adaptador extends RecyclerView.Adapter<adaptador.ViewHolder> {

    List<pais> ListDatos;
    Context context;

    public adaptador(List<pais> listDatos, Context context) {
        ListDatos = listDatos;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlist, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.dato1.setText(ListDatos.get(position).getNombre());
       // holder.dato2.setText(ListDatos.get(position).getNombre2());
        Picasso.with(getContext()).load(ListDatos.get(position).getUrl()).into(holder.img);
       // System.out.println(ListDatos.get(position).getUrl());
    }

    @Override
    public int getItemCount() {
        return ListDatos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private  TextView dato1,dato2;
        private  ImageView img;

         ViewHolder(@NonNull View itemView) {
            super(itemView);
            dato1=(TextView) itemView.findViewById(R.id.textNombre);
            dato2=(TextView)itemView.findViewById(R.id.textApellido);
            img=(ImageView) itemView.findViewById(R.id.ivMovieImage);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {

            pais movie = ListDatos.get(getAdapterPosition());

            //Toast.makeText(getContext(),movie.getNombre1(),Toast.LENGTH_LONG).show();

           Intent intent = new Intent(getContext(), detalle.class);
           intent.putExtra("Pais", movie);
            getContext().startActivity(intent);

        }


    }
}
