package com.example.paises.enetity;

import java.io.Serializable;

public class pais implements Serializable {
    String nombre;
    String himno;
    String url;
    String videoUrl;
    String capital;
    String poblacion;

    public pais(String nombre,String videoUrl ,String himno, String url, String capital, String poblacion) {
        this.nombre = nombre;
        this.himno = himno;
        this.url = url;
        this.videoUrl = videoUrl;
        this.capital = capital;
        this.poblacion = poblacion;
    }

    public String getHimno() {
        return himno;
    }

    public void setHimno(String himno) {
        this.himno = himno;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
