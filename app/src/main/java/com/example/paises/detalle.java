package com.example.paises;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.paises.enetity.pais;
import com.squareup.picasso.Picasso;

public class detalle extends AppCompatActivity  {



    String direccion;
    TextView nombre;
    TextView capital;
    TextView poblacion;
    WebView webView;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);


        nombre=(TextView) findViewById(R.id.nombrePais);
        capital=(TextView) findViewById(R.id.capital);
        poblacion=(TextView) findViewById(R.id.poblacion);
        img=(ImageView) findViewById(R.id.pais);
        webView=(WebView) findViewById(R.id.video);

        direccion = "";


        Bundle recibe=getIntent().getExtras();
        pais p = (pais) recibe.getSerializable("Pais");
        nombre.setText(p.getNombre().toString());
        capital.setText(p.getCapital().toString());
        poblacion.setText(p.getPoblacion().toString());
        webView.loadData(p.getVideoUrl(),"text/html","utf-8");
        Picasso.with(img.getContext()).load(p.getUrl()).into(img);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
        });
        webView.loadData(p.getVideoUrl(), "text/html" , "utf-8");
      //  tx.setText(direccion);
        //tx.setOnClickListener(this);

    }


    public void irWeb(String d){
        Uri uri = Uri.parse(d);
        Intent intenNav =   new Intent(Intent.ACTION_VIEW,uri);
        startActivity(intenNav);

    }
}
